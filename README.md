# Tarea1
* Luis Alonso Rodríguez Picado
* B76547
* Estructuras de computadoras II

## Descripción del programa
El programa consiste en la simulación de multiples métodos de predictores de saltos. Contiene los predictores de tipo Bimodal, Private history table, Global history table, y Tournament. 
Es programado en C++ y utiliza las siguientes bibliotecas: 
- include iostream
- include string
- include fstream
- include sstream
- include math.h
- include stdio.h
- include cstring
- include stddef.h

Para ejecutarlo, se corre de la siguiente forma: 
gunzip -c branch-trace-gcc.trace.gz | branch -s < # > -bp < # > -gh < # > -ph < # >

Donde  branch-trace-gcc.trace.gz es el archivo comprimido de los branches. Luego (-s) es la cantidad de bits del BHT, (-bp) el tipo de predictor [0 Bimodal, 1 PHT, 2 GHT, 3 Tournament], (-gh) es el tamaño del registro y (-ph) es el tamaño de cada entrada del PHT.
