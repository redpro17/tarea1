#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <math.h>
#include <stdio.h>
#include <cstring>
#include <stddef.h>

/*Código de predictores de saltos */
using namespace std;

// Setup del programa, se asignan los valores a las variables globaes dependiendo de las entradas del usuario:
int type = 0; //Tipo de predición utilizada numéricamente
string branch; // Tipo de predición utilizada para asignar el string
int s = 0;  //Tamaño del Branch history Table
int Global = 0; // Tamaño del Global history register
int Private = 0; // Tamaño del Private history register
// Variables globales para asignar resultados: 
int jumps = 0; // Saltos totales
// Predicciones correctas e incorrectas
int correctT = 0;
int incorrectT = 0;
int correctN = 0;
int incorrectN = 0;
float percentage = 0.0; // Porcentaje de saltos acertados

//Función para insertar bits, recibe el entero en el que se inserta el bit, la posición donde se entrega, y si es un alto o un bajo
// Devuelve el entero con el bit modificado
template <typename T> 
T insert_bit(T n,   
             size_t position, 
             bool new_bit) 
{
    T x = n;
    T y = x;
    x <<= 1;
    if (new_bit)
        x |= (((T) 1) << position);
    else
        x &= ~(((T) 1) << position);
    x &= ((~((T) 0)) << position);
    y &= ~((~((T) 0)) << position);
    x |= y;
    return x;
}

//Función para debugear el BHT. Recibe el array del BHT y su tamaño. Retorna 0, e imprime el contenido. 
int debugBHT(int* arrayBHT, int lenght){
    for (int i=0; i<lenght; i++){
        if(arrayBHT[i] == 0){
            cout<<"N";
        }
        if(arrayBHT[i] == 1){
            cout<<"n";
        }
        if(arrayBHT[i] == 2){
            cout<<"T";
        }
        if(arrayBHT[i] == 3){
            cout<<"t";
        }
    }
    cout << "\n";
    return 0;
}

//Función que recibe el salto correcto y la predición y los compara imprimiendo si fue correcto o no. Retorna 0.
int debugger(char Correct, char Prediction){
    cout << "Predición correcta:" << Correct << "       Predicción dada:" << Prediction << "  ";
    if(Correct == Prediction){
        cout << "Correct  \n";
    }
    else{
        cout << "Incorrect  \n";
    }
    return 0;
}
// Función para convertir a binario un número. Imprime el número en binario.
void bin(unsigned n) 
{ 
    /* step 1 */
    if (n > 1) 
        bin(n/2); 
  
    /* step 2 */
    cout << n % 2; 
} 


// Función para crear una mascara que permita recortar los bits de un entero. Retorna el numero de la máscara
unsigned createMask(unsigned a, unsigned b)
{
   unsigned r = 0;
   for (unsigned i=a; i<=b; i++)
       r |= 1 << i;

   return r;
}

// Función para la predicción del Private history table, recibe la cantidad de bits de BHT y la cantidad de bits
// Por entrada del PHT
unsigned PHT(int s, int Private){
    // Se inicializan las variables necesarias
    unsigned lenght = pow(2,s);
    unsigned bits_per_entry = pow(2, Private); 
    unsigned arrayPHT[lenght];
    unsigned arrayBHT[lenght];
    for( int i=0; i< lenght ; i++){
        arrayBHT[i] = 0;
        arrayPHT[i] = 0;
    }
    long int PC;
    char bp;
    int contador = 1;
    while(scanf("%ld %c \n", &PC, &bp) != EOF){ //Se abre el archivo y se recorren todas las lineas
        unsigned mask = createMask(0,s-1);
        unsigned mask2 = createMask(0,Private-1);
        unsigned select = PC & mask;
        char debug;
        jumps++;
        unsigned PHTContent = arrayPHT[select] & mask2;
        //Limitamos el tamaño del PHT a los bits solicitados
        if(PHTContent >= bits_per_entry){
                arrayPHT[select] = bits_per_entry-1;
            }
        // XOR entre los Private bits de PHT y los s bits del PC
        unsigned selectBHT = PHTContent ^ select;
        // A partir de aquí, se realizan comparaciones para analizar cuando coinciden las predicciones
        // Y se actualizan las tablas respectivamente
        if(bp == 'T'){
            
            
            
            arrayPHT[select] = insert_bit(arrayPHT[select],0,true);


            if(arrayBHT[selectBHT] == 1 || arrayBHT[selectBHT] == 0){
                debug = 'N';
                incorrectT++;
                arrayBHT[selectBHT]= arrayBHT[selectBHT] +1;
                if(arrayBHT[selectBHT] > 3){
                    arrayBHT[selectBHT] = 3;
                }
            }
            else if(arrayBHT[selectBHT] == 2 || arrayBHT[selectBHT] == 3){
                debug = 'T';
                correctT++;
                arrayBHT[selectBHT] = arrayBHT[selectBHT]+1;
                if(arrayBHT[selectBHT] > 3){
                    arrayBHT[selectBHT] = 3;
                }
            }
        }
        else if(bp == 'N'){
            arrayPHT[select] = insert_bit(arrayPHT[select],0,false);
            if(arrayBHT[selectBHT] == 1 || arrayBHT[selectBHT] == 0){
                debug = 'N';
                correctN++;

                if(arrayBHT[selectBHT] > 0){
                    arrayBHT[selectBHT] = arrayBHT[selectBHT] -1;
                }
                

            }
            else if(arrayBHT[selectBHT] == 2 || arrayBHT[selectBHT] == 3){
                debug = 'T';
                incorrectN++;
                if(arrayBHT[selectBHT] > 0){
                    arrayBHT[selectBHT] = arrayBHT[selectBHT] -1;
                }
                
            }
            
            
        }

    }
    return 0;

}

// Función para la predicción del Global history table, recibe la cantidad de bits de BHT y la cantidad de bits
// del GHT
unsigned GHT(int s, int Global){
    // Se asignan las variables necesarias para ejecutar el GHT, además de inicializar los vectores
    unsigned lenght = pow(2,s);
    int limit_register = pow(2,Global);
    unsigned global_register = 0;
    int arrayBHT[lenght];
    for( int i=0; i< lenght ; i++){
        arrayBHT[i] = 0;
    }
    long int PC;
    char bp;
    int counter = 1;
    while(scanf("%ld %c \n", &PC, &bp) != EOF){ //Se abre el archivo y se recorren todas las lineas
        //Se inicializan las variables necesarias
        unsigned mask = createMask(0,s-1);
        char debug;
        unsigned mask2 = createMask(0,Global-1);
        unsigned verification = global_register & mask2;
        global_register = verification;

        jumps++;
        // Bitwise XOR entre el PC y el registro global
        unsigned XOR = PC ^ global_register;
        //Se extraen los ultimos bits con la máscara
        unsigned select = XOR & mask;
        int prediction = arrayBHT[select];
        // A partir de aquí, se realizan comparaciones para analizar cuando coinciden las predicciones
        // Y se actualizan la tabla y el registro según corresponda
        if(bp == 'N'){


            if(prediction <= 1){
                correctN++;
                debug = 'N';
              
                arrayBHT[select] = arrayBHT[select]-1;
              
                if(arrayBHT[select] < 0){
                    arrayBHT[select] = 0;
                }
            }else if(prediction >= 2){
                incorrectT++;
                debug = 'T';
              
                arrayBHT[select] = arrayBHT[select]-1;
              
                if(arrayBHT[select] < 0){
                    arrayBHT[select] = 0;
                }
            }


            

            global_register =insert_bit(global_register, 0, false);
            
        }else if(bp == 'T'){


            if(prediction <= 1){
                debug = 'N';
                incorrectN++;
              
                arrayBHT[select] = arrayBHT[select]+1;
              
                if(arrayBHT[select] > 3){
                    arrayBHT[select] = 3;
                }
            }else if(prediction >= 2){
                debug = 'T';
                correctT++;
              
                arrayBHT[select] = arrayBHT[select]+1;
               
                if(arrayBHT[select] > 3){
                    arrayBHT[select] = 3;
                }
            }

            global_register = insert_bit(global_register,0,true);
        }

    }
    return 0;
}
// Función para la predicción por torneo, recibe la cantidad de bits de BHT, la cantidad de bits
// del GHT y la cantidad de bits por entrada del private history table. Retorna 0.

unsigned Tournament(int s, int Global, int Private){
// Se reutiliza el código de PHT y GHT para obtener la predicción del Tournament:
// Código PHT, Variables:
    unsigned lenght = pow(2,s);
    unsigned bits_per_entry = pow(2, Private); 
    unsigned arrayPHT[lenght];
    unsigned arrayBHT[lenght];
    int arrayBHT_GHT[lenght];
    char predictionPHT;
    char predictionGHT;
    unsigned TournamentArray[lenght];
    for( int i=0; i< lenght ; i++){
        arrayBHT[i] = 0;
        arrayPHT[i] = 0;
        arrayBHT_GHT[i] = 0;
        TournamentArray[i] = 0;
    }
// Variables para GHT:
    int limit_register = pow(2,Global);
    unsigned global_register = 0;
    long int PC;
    char bp;
    int counter = 1;
    
    while(scanf("%ld %c \n", &PC, &bp) != EOF){ //Se abre el archivo y se recorren todas las lineas

    // Código PHT:
        unsigned mask = createMask(0,s-1);
        unsigned mask2 = createMask(0,Private-1);
        unsigned select = PC & mask;
        char debug;
        jumps++;
        unsigned PHTContent = arrayPHT[select] & mask2;

        if(PHTContent >= bits_per_entry){
                arrayPHT[select] = bits_per_entry-1;
            }
        
        unsigned selectBHT = PHTContent ^ select;
        if(bp == 'T'){
            //Limitamos el tamaño del PHT a los bits solicitados
            
            
            arrayPHT[select] = insert_bit(arrayPHT[select],0,true);


            if(arrayBHT[selectBHT] == 1 || arrayBHT[selectBHT] == 0){
                debug = 'N';
                predictionPHT = 'N';
                arrayBHT[selectBHT]= arrayBHT[selectBHT] +1;
                if(arrayBHT[selectBHT] > 3){
                    arrayBHT[selectBHT] = 3;
                }
            }
            else if(arrayBHT[selectBHT] == 2 || arrayBHT[selectBHT] == 3){
                debug = 'T';
                predictionPHT = 'T';
                arrayBHT[selectBHT] = arrayBHT[selectBHT]+1;
                if(arrayBHT[selectBHT] > 3){
                    arrayBHT[selectBHT] = 3;
                }
            }
        }
        else if(bp == 'N'){
            arrayPHT[select] = insert_bit(arrayPHT[select],0,false);
            if(arrayBHT[selectBHT] == 1 || arrayBHT[selectBHT] == 0){
                predictionPHT = 'N';
                debug = 'N';
                if(arrayBHT[selectBHT] > 0){
                    arrayBHT[selectBHT] = arrayBHT[selectBHT] -1;
                }
                

            }
            else if(arrayBHT[selectBHT] == 2 || arrayBHT[selectBHT] == 3){
                predictionPHT = 'T';
                debug = 'T';
                if(arrayBHT[selectBHT] > 0){
                    arrayBHT[selectBHT] = arrayBHT[selectBHT] -1;
                }
                
            }
            
        }

// Código GHT:
        mask = createMask(0,s-1);
        mask2 = createMask(0,Global-1);
        unsigned verification = global_register & mask2;
        global_register = verification;

        unsigned XOR = PC ^ global_register;
        select = XOR & mask;
        int prediction = arrayBHT_GHT[select];
        
        if(bp == 'N'){


            if(prediction <= 1){
                predictionGHT = 'N';
                debug = 'N';
                arrayBHT_GHT[select] = arrayBHT_GHT[select]-1;
                if(arrayBHT_GHT[select] < 0){
                    arrayBHT_GHT[select] = 0;
                }
            }else if(prediction >= 2){
                predictionGHT = 'T';
                debug = 'T';
                arrayBHT_GHT[select] = arrayBHT_GHT[select]-1;
                if(arrayBHT_GHT[select] < 0){
                    arrayBHT_GHT[select] = 0;
                }
            }


            

            global_register =insert_bit(global_register, 0, false);
            
        }else if(bp == 'T'){


            if(prediction <= 1){
                debug = 'N';
                predictionGHT = 'N';
                arrayBHT_GHT[select] = arrayBHT_GHT[select]+1;
                if(arrayBHT_GHT[select] > 3){
                    arrayBHT_GHT[select] = 3;
                }
            }else if(prediction >= 2){
                debug = 'T';
                predictionGHT = 'T';
                arrayBHT_GHT[select] = arrayBHT_GHT[select]+1;
                if(arrayBHT_GHT[select] > 3){
                    arrayBHT_GHT[select] = 3;
                }
            }

            global_register = insert_bit(global_register,0,true);
        }
// Código del metapredictor:
        // Se verifica que las variables tengan el valor deseado
        mask = createMask(0,s-1);
        select = PC & mask;
        // Se guarda la predicción del metapredictor, y se compara posteriormente el resultado correcto
        // Con el resultado del predictor seleccionado. Si coinciden o no, se actualiza según corresponda el Metapredictor
        int Tournament_Predictor_Selection = TournamentArray[select];
        char Final_Predict;

        if (Tournament_Predictor_Selection <= 1 ){
            Final_Predict = predictionGHT;
            if (Final_Predict == bp && predictionGHT != predictionPHT){
                if(TournamentArray[select] > 0){
                    TournamentArray[select] = TournamentArray[select]-1;
                }
            }else if(predictionGHT != predictionPHT){
                TournamentArray[select] = TournamentArray[select]+1;
            }
        }else if (Tournament_Predictor_Selection >= 2 ){
            Final_Predict = predictionPHT;
            if (Final_Predict == bp && predictionGHT != predictionPHT){
                TournamentArray[select] = TournamentArray[select]+1;
                if(TournamentArray[select] > 3){
                    TournamentArray[select] = 3;
                }
            }else if(predictionGHT != predictionPHT){
                TournamentArray[select] = TournamentArray[select]-1;
            }
        }
        // Finalmente se compara la decisión final con la correcta para dar el resultado.

        if (Final_Predict == bp && bp == 'T'){
            correctT++;
        }
        if (Final_Predict == bp && bp == 'N'){
            correctN++;
        }
        if (Final_Predict != bp && bp == 'T'){
            incorrectT++;
        }
        if (Final_Predict != bp && bp == 'N'){
            incorrectN++;
        }

        

    }



    return 0;
}


// Función para la predicción bimodal, recibe la cantidad de bits de BHT. Retorna 0.
unsigned Bimodal(int s){
    // Predictor Bimodal
        int lenght = (int)pow(2, s);
        int array[lenght];
        for (int i = 0; i<lenght; i++){ 
            array[i] = 0;
        }
        long int PC;
        char bp;

        while(scanf("%ld %c \n", &PC, &bp) != EOF){ //Se abre el archivo y se recorren todas las lineas
            unsigned mask = createMask(0,s-1);
            char debug;
            int select = PC & mask;
           
            //Analisis de la predicción Not taken
            jumps++;
            
            if(array[select] <= 1){
                
                if(bp == 'N'){
                    debug = 'N';
                    correctN++;
                    array[select] -= 1;
                    if(array[select] < 0){
                        array[select] = 0;
                    }
                }
                else
                {
                    debug = 'N';
                    incorrectT++;
        
                    array[select] += 1;

                    if(array[select] > 3){
                        array[select] = 3;
                    }
                }
                
            }
        
            //Analisis de la predicción Taken
            else if(array[select] >= 2 ){
        
                if(bp == 'T'){
                    debug = 'T';
                    correctT++;
                    array[select] += 1;
                    if(array[select] > 3){
                        array[select] = 3;
                    }
                }
                else
                {
                    debug = 'T';
                    incorrectN++;
        
                    array[select] -= 1;
                    if(array[select] < 0){
                        array[select] = 0;
                    }
                }
                
            }
        }
        return 0;

}


int main(int argc, char* argv[]){
    char line[100];

    int counter = 1;
    while(counter < argc){ // Se obtienen los parámetros brindados por el usuario 
        char* param = argv[counter];
        if(strcmp(param, "-s") == 0){
            s = stoi(argv[counter+1]);
        }
        if(strcmp(param, "-bp") == 0){
            type = stoi(argv[counter+1]);
        }
        if(strcmp(param, "-gh") == 0){
            Global = stoi(argv[counter+1]);
        }
        if(strcmp(param, "-ph") == 0){
            Private = stoi(argv[counter+1]);
        }

        counter++;
    }
// Se asigna el tipo de predicción para los resultados, se ejecuta la función según la entrada
//********************** Aqui se trabajan las predicciones según el tipo seleccionado ******************************************

    if(type == 0){
    branch = "Predictor Bimodal\n";
        Bimodal(s);
    }
    if(type == 1){
        branch = "Predictor con Historia Privada\n";
        PHT(s, Private);
    }
    if(type == 2){
        branch = "Predictor con historia Global\n";
        GHT(s, Global);
    }
    if(type == 3){
        branch = "Predictor con torneo\n";
        Tournament(s,Global, Private);
    }


    


// Sección donde se imprimen los resultados en consola
    float denom = correctN+correctT+incorrectN+incorrectT;
    float num = correctN+correctT;
    percentage = num/denom;
    percentage = percentage*100;
    cout << "------------------------------------------------------------------------\n";
    cout << "\n";
    cout << "Prediction parameters\n: ";
    cout << "\n";
    cout << "------------------------------------------------------------------------\n";
    cout << "\n";
    cout << "Branch prediction type: " <<  branch << "\n"; 
    cout << "\n";
    cout << "BHT size (entries): " << pow(2,s) << "\n";
    cout << "\n";
    cout << "Global history register size: " << Global << "\n";
    cout << "\n";
    cout << "Private history register size: " << Private << "\n";
    cout << "\n";
    cout << "------------------------------------------------------------------------\n" ;
    cout << "\n";
    cout << "------------------------------------------------------------------------\n" ;
    cout << "\n";
    cout << "Simulation results:\n" ;
    cout << "\n";
    cout << "------------------------------------------------------------------------\n" ;
    cout << "\n";
    cout << "Number of branches: " << jumps << "\n" ;
    cout << "\n";
    cout << "Number of correct predictions of taken branches: " << correctT << "\n" ;
    cout << "\n";
    cout << "Number of incorrect predictions of taken branches: " << incorrectT << "\n" ;
    cout << "\n";
    cout << "Number of correct predictions of not-taken branches: " << correctN << "\n" ;
    cout << "\n";
    cout << "Number of incorrect predictions of not-taken branches: " << incorrectN << "\n" ;
    cout << "\n";
    cout << "Percentage of correct predictions: " << percentage << "%\n" ;
    cout << "\n";
    cout << "------------------------------------------------------------------------\n" ;


    fclose(stdin);
    return 0;

}